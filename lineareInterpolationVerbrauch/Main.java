package lineareInterpolationVerbrauch;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Main 
{
	static Connection c;
	
	//x....datum
	//y....value
	static double maximalerWertY;
	static long maximalerWertX;
	static long minimalerWertX;
	static double minimalerWertY;
	
	
	public static void main(String[] args) throws SQLException, ClassNotFoundException 
	{
		double durchschnittStrom = 0;
		double durchschnittWasser = 0;
		DatenbankVerbindung();
		//J�nner
			getMaxStrom(0);
			getMinStrom(0);
			Date date = new Date(2016,0,1);
			long x = date.getSeconds();
			x = x/1000;
			double y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittStrom += y;
			System.out.println("1.J�nner Strom: " + y);
			
			
			getMaxWasser(0);
			getMinWasser(0);
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittWasser += y;
			System.out.println("1.J�nner Wasser: " + y);
			
		//Februar
			getMaxStrom(1);
			getMinStrom(1);
			date = new Date(2016,1,1);
			x = date.getSeconds();
			x = x/1000;
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittStrom += y;
			System.out.println("1.Februar Strom: " + y);
			
			
			getMaxWasser(1);
			getMinWasser(1);
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittWasser += y;
			System.out.println("1.Februar Wasser: " + y);
			
		//M�rz
			getMaxStrom(2);
			getMinStrom(2);
			date = new Date(2016,2,1);
			x = date.getSeconds();
			x = x/1000;
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittStrom += y;
			System.out.println("1.M�rz Strom: " + y);
			
			
			getMaxWasser(2);
			getMinWasser(2);
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittWasser += y;
			System.out.println("1.M�rz Wasser: " + y);
			
		//April
			getMaxStrom(3);
			getMinStrom(3);
			date = new Date(2016,3,1);
			x = date.getSeconds();
			x = x/1000;
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittStrom += y;
			System.out.println("1.April Strom: " + y);
			
			
			getMaxWasser(3);
			getMinWasser(3);
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittWasser += y;
			System.out.println("1.April Wasser: " + y);
		//Mai
			getMaxStrom(4);
			getMinStrom(4);
			date = new Date(2016,4,1);
			x = date.getSeconds();
			x = x/1000;
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittStrom += y;
			System.out.println("1.Mai Strom: " + y);
			
			
			getMaxWasser(4);
			getMinWasser(4);
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittWasser += y;
			System.out.println("1.Mai Wasser: " + y);
		//Juni
			getMaxStrom(5);
			getMinStrom(5);
			date = new Date(2016,5,1);
			x = date.getSeconds();
			x = x/1000;
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittStrom += y;
			System.out.println("1.Juni Strom: " + y);
			
			
			getMaxWasser(5);
			getMinWasser(5);
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittWasser += y;
			System.out.println("1.Juni Wasser: " + y);
		//Juli
			getMaxStrom(6);
			getMinStrom(6);
			date = new Date(2016,6,1);
			x = date.getSeconds();
			x = x/1000;
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittStrom += y;
			System.out.println("1.Juli Strom: " + y);
			
			
			getMaxWasser(6);
			getMinWasser(6);
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittWasser += y;
			System.out.println("1.Juli Wasser: " + y);
		//August
			getMaxStrom(7);
			getMinStrom(7);
			date = new Date(2016,7,1);
			x = date.getSeconds();
			x = x/1000;
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittStrom += y;
			System.out.println("1.August Strom: " + y);
			
			
			getMaxWasser(7);
			getMinWasser(7);
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittWasser += y;
			System.out.println("1.August Wasser: " + y);
		//September
			getMaxStrom(8);
			getMinStrom(8);
			date = new Date(2016,8,1);
			x = date.getSeconds();
			x = x/1000;
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittStrom += y;
			System.out.println("1.September Strom: " + y);
			
			
			getMaxWasser(8);
			getMinWasser(8);
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittWasser += y;
			System.out.println("1.September Wasser: " + y);
		//Oktober
			getMaxStrom(9);
			getMinStrom(9);
			date = new Date(2016,9,1);
			x = date.getSeconds();
			x = x/1000;
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittStrom += y;
			System.out.println("1.Oktober Strom: " + y);
			
			
			getMaxWasser(9);
			getMinWasser(9);
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittWasser += y;
			System.out.println("1.Oktober Wasser: " + y);
		//November
			getMaxStrom(10);
			getMinStrom(10);
			date = new Date(2016,10,1);
			x = date.getSeconds();
			x = x/1000;
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittStrom += y;
			System.out.println("1.November Strom: " + y);
			
			
			getMaxWasser(10);
			getMinWasser(10);
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittWasser += y;
			System.out.println("1.November Wasser: " + y);
		//Dezember
			getMaxStrom(11);
			getMinStrom(11);
			date = new Date(2016,11,1);
			x = date.getSeconds();
			x = x/1000;
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittStrom += y;
			System.out.println("1.Dezember Strom: " + y);
			
			
			getMaxWasser(11);
			getMinWasser(11);
			y = Interpolation(minimalerWertX, minimalerWertY, maximalerWertX, maximalerWertY, x);
			durchschnittWasser += y;
			System.out.println("1.Dezember Wasser: " + y);
			
			System.out.println("Durchschnittlicher Wasserverbrauch: " + durchschnittWasser/12);
			System.out.println("Durchschnittlicher Stromverbrauch: " + durchschnittStrom/12);
	}
	
	public static double Interpolation(long x1, double y1, long x2, double y2, long xErstenDesMonats)
	{
		double verbrauch;
		
		long dx = Math.abs(x1-x2);
		double dy = Math.abs(y1-y2);
		
		double k = dy/dx;
		double d = y1 - x1*(dy/dx);
		
		verbrauch = k*xErstenDesMonats + d;
		
		return verbrauch;
	}
	
	public static void DatenbankVerbindung()
	{
		try {
		      Class.forName("org.sqlite.JDBC");
		      c = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/verbrauch.db");
		      c.setAutoCommit(false);
		      System.out.println("Opened database successfully");
		      
		    } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
		    }
	}
	
	//int month = 0-11 
	//0....January
	public static void getMaxStrom(int month) throws SQLException, ClassNotFoundException
	{
		Class.forName("org.sqlite.JDBC");
		c= DriverManager.getConnection("jdbc:sqlite:C:/sqlite/verbrauch.db");
		
		ArrayList<Long> daten = new ArrayList<Long>(); 
		
		String sql = "SELECT * FROM strom_verbrauch";
		Statement stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		while (rs.next())
		{
			long datum = rs.getInt("datum");
			datum = datum * 1000;
			Date d = new Date(datum);
			if(d.getMonth() == month)
			{
				daten.add(datum);
				for(int i = 0; i<daten.size(); i++)
				{
					long datum2 = daten.get(i);
					
					if(maximalerWertX < datum2)
					{
						maximalerWertX = datum2;
						String sql2  ="Select * FROM strom_verbrauch WHERE datum = ?";
						PreparedStatement stmt2 = c.prepareStatement(sql2);
						stmt2.setDouble(1, maximalerWertX);
						ResultSet rs2 = stmt2.executeQuery(sql2);
						rs2.next();
						maximalerWertY = rs2.getDouble("value");
						stmt2.executeUpdate();
						stmt2.close();
						rs2.close();
					}
				}
			}
		}
		rs.close();
		stmt.close();
	}
	
	//int month = 0-11 
	//0....January
	public static void getMinStrom(int month) throws SQLException, ClassNotFoundException
	{
		Class.forName("org.sqlite.JDBC");
		c= DriverManager.getConnection("jdbc:sqlite:C:/sqlite/verbrauch.db");
		String sql = "SELECT * FROM strom_verbrauch";
		Statement stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		while (rs.next())
		{
			long datum = rs.getLong("datum");
			datum = datum * 1000;
			Date d = new Date(datum);
			if(d.getMonth() == month)
			{
				minimalerWertX = datum;
				String sql2  ="Select * FROM strom_verbrauch WHERE datum = ?";
				PreparedStatement stmt2 = c.prepareStatement(sql2);
				stmt2.setLong(1, datum);
				ResultSet rs2 = stmt2.executeQuery(sql2);
				rs2.next();
				minimalerWertY = rs2.getDouble("value");
				stmt2.executeUpdate();
				rs2.close();
				stmt2.close();
				break;
			}
		}
		rs.close();
		stmt.close();
	}
	
	
	
		//int month = 0-11 
		//0....January
		public static void getMaxWasser(int month) throws SQLException, ClassNotFoundException
		{
			Class.forName("org.sqlite.JDBC");
			c= DriverManager.getConnection("jdbc:sqlite:C:/sqlite/verbrauch.db");
			
			ArrayList<Long> daten = new ArrayList<Long>(); 
			
			String sql = "SELECT * FROM wasser_verbrauch";
			Statement stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			while (rs.next())
			{
				long datum = rs.getInt("datum");
				datum = datum * 1000;
				Date d = new Date(datum);
				if(d.getMonth() == month)
				{
					daten.add(datum);
					for(int i = 0; i<daten.size(); i++)
					{
						long datum2 = daten.get(i);
						
						if(maximalerWertX < datum2)
						{
							maximalerWertX = datum2;
							String sql2  ="Select * FROM wasser_verbrauch WHERE datum = ?";
							PreparedStatement stmt2 = c.prepareStatement(sql2);
							stmt2.setDouble(1, maximalerWertX/1000);
							ResultSet rs2 = stmt2.executeQuery();
							rs2.next();
							maximalerWertY = rs2.getDouble("value");
							stmt2.executeUpdate();
							stmt2.close();
							rs2.close();
						}
					}
				}
			}
			rs.close();
			stmt.close();
		}
		
		
		//int month = 0-11 
		//0....January
		public static void getMinWasser(int month) throws SQLException, ClassNotFoundException
		{
			Class.forName("org.sqlite.JDBC");
			c= DriverManager.getConnection("jdbc:sqlite:C:/sqlite/verbrauch.db");
			String sql = "SELECT * FROM wasser_verbrauch";
			Statement stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			while (rs.next())
			{
				int datum = rs.getInt("datum");
				datum = datum * 1000;
				Date d = new Date(datum);
				if(d.getMonth() == month)
				{
					minimalerWertX = datum;
					String sql2  ="Select * FROM wasser_verbrauch WHERE datum = ?";
					PreparedStatement stmt2 = c.prepareStatement(sql2);
					stmt2.setInt(1, datum);
					ResultSet rs2 = stmt2.executeQuery(sql2);
					rs2.next();
					minimalerWertY = rs2.getInt("value");
					stmt2.executeUpdate();
					stmt2.close();
					rs2.close();
					break;
				}
			}
			rs.close();
			stmt.close();
		}
	
}
