import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Scanner;

public class Poker {

	static final int anzahlGleicherFarbe = 13;
	static final int anzahlFarben = 4;
	static final int prozent = 100;
	


	public static void main(String[] args) throws SQLException, ClassNotFoundException {

		double anzahlWieOft;
		float royalFlush = 0;
		float straightFlush = 0;
		float flush = 0;
		float poker = 0;
		float drilling = 0;
		float zweiPaar = 0;
		float paar = 0;
		float fullHouse = 0;
		float straight = 0;
		
		int[] gezogeneKarten = new int[5];
		int[] karten = new int[anzahlFarben * anzahlGleicherFarbe];
		
		for (int i = 0; i < karten.length; i++) {
			karten[i] = i + 1;
		}

		
		
		Scanner s = new Scanner(System.in);
		System.out.println("Bitte geben Sie die Anzahl an Runden ein: ");
		anzahlWieOft = s.nextDouble();
		
		long unixTime = System.currentTimeMillis() / 1000;
		
		for (int i = 1; i <= anzahlWieOft; i++) {
			gezogeneKarten = karteZiehen(karten);

			if (findeRoyalFlush(gezogeneKarten) == true) {
				royalFlush++;
			}
			if (findeStraightFlush(gezogeneKarten) == true) {
				straightFlush++;
			}
			if (findePoker(gezogeneKarten) == true) {
				poker++;
			}
			if (findeFullHouse(gezogeneKarten) == true) {
				fullHouse++;
			}
			if (findeFlush(gezogeneKarten) == true) {
				flush++;
			}
			if (findeStra�e(gezogeneKarten) == true) {
				straight++;
			}
			if (findeDrilling(gezogeneKarten) == true) {
				drilling++;
			}
			if (finde2Paar(gezogeneKarten) == true) {
				zweiPaar++;
			}
			if (findePaar(gezogeneKarten) == true) {
				paar++;
			}
		}
		
		System.out.println("Um: " + unixTime + " s " + "sind von " + anzahlWieOft + " Versuchen"+", folgende prozentuelle Ergebnisse enstanden Ergebnisse entstanden:" + "\n" 
				+ "Paar: " + (paar*prozent)/anzahlWieOft + "%" + "\n" 
				+ "2 Paare: " + (zweiPaar*prozent)/anzahlWieOft + "%" + "\n" 
				+ "Drilling: " + (drilling*prozent)/anzahlWieOft + "%" + "\n" 
				+ "Stra�e: " + (straight*prozent)/anzahlWieOft + "%" + "\n" 
				+ "Flush: " + (flush*prozent)/anzahlWieOft+ "%" + "\n" 
				+ "Full House: " + (fullHouse*prozent)/anzahlWieOft + "%" + "\n" 
				+ "Poker: " + (poker*prozent)/anzahlWieOft + "%" + "\n" 
				+ "Straight Flush: " + (straightFlush*prozent)/anzahlWieOft + "%" + "\n" 
				+ "Royal Flush: " + (royalFlush*prozent)/anzahlWieOft + "%");
		
		
		
		Connection c = null;
		Statement stmt = null;
	    try {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/poker.db");
	      c.setAutoCommit(false);
	      System.out.println("Opened database successfully");
	      
	      stmt = c.createStatement();
	      String sql = "INSERT INTO results VALUES(" 
	    		  +unixTime +", "
	    		  +anzahlWieOft+", "
	    		  +paar+", "
	    		  +drilling +", "
	    		  +poker+", "
	    		  +flush+", "
	    		  +fullHouse+", "
	    		  +zweiPaar+", "
	    		  +straight+", "
	    		  +straightFlush+", "
	    		  +royalFlush+");";
	      stmt.executeUpdate(sql);
	      stmt.close();
	      c.commit();
	      c.close();
	      
	    } catch ( Exception e ) {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
	    
		
		
	}

	// hier werden 5 zuf�llige Pokerkarten gezogen
	public static int[] karteZiehen(int karten []) {
		int[] gezogen = new int[5];

		for (int j = 0; j < 5; j++) {
			int stelle = (int) (Math.random() * (52 - (j + 1)));
			gezogen[j] = karten[stelle];
			karten[stelle] = karten[52 - (j + 1)];
			karten[52 - (j + 1)] = gezogen[j];
		}
		return gezogen;
	}

	// Es wird der wahre Wert der Karte ermittelt.
	public static int wahrerWertDerKarte(int karte) {
		return karte % anzahlGleicherFarbe;
	}

	// es wird die Farbe der Karte ermittelt (es gibt 4 Farben)
	public static int wahreFarbeDerKarte(int karte) {
		return karte / anzahlGleicherFarbe;
	}

	// hier wird ein paar gesucht
	public static boolean findePaar(int karte []) {
		for (int i = 0; i < karte.length - 1; i++) {
			for (int j = i + 1; j < karte.length; j++) {
				if (wahrerWertDerKarte(karte[i]) == wahrerWertDerKarte(karte[j])) {
					return true;
				}
			}
		}
		return false;
	}

	// hier wird ein Drilling gesucht
	public static boolean findeDrilling(int karte []) {
		for (int i = 0; i < karte.length - 2; i++) {
			for (int j = i + 1; j < karte.length - 1; j++) {
				for (int k = j + 1; k < karte.length; k++) {
					if (wahrerWertDerKarte(karte[i]) == wahrerWertDerKarte(karte[j])
							&& wahrerWertDerKarte(karte[j]) == wahrerWertDerKarte(karte[k])) {
						return true;
					}
				}
			}
		}
		return false;
	}

	// hier wird ein Poker gesucht
	public static boolean findePoker(int hand []) {

		if (wahrerWertDerKarte(hand[0]) == wahrerWertDerKarte(hand[1])
				&& (wahrerWertDerKarte(hand[2]) == wahrerWertDerKarte(hand[3]))
				&& (wahrerWertDerKarte(hand[1]) == wahrerWertDerKarte(hand[2]))) {
			return true;
		} else if (wahrerWertDerKarte(hand[1]) == wahrerWertDerKarte(hand[2])
				&& (wahrerWertDerKarte(hand[3]) == wahrerWertDerKarte(hand[4]))
				&& (wahrerWertDerKarte(hand[2]) == wahrerWertDerKarte(hand[3]))) {
			return true;
		} else if (wahrerWertDerKarte(hand[0]) == wahrerWertDerKarte(hand[1])
				&& (wahrerWertDerKarte(hand[3]) == wahrerWertDerKarte(hand[4]))
				&& (wahrerWertDerKarte(hand[1]) == wahrerWertDerKarte(hand[3]))) {
			return true;
		} else if (wahrerWertDerKarte(hand[0]) == wahrerWertDerKarte(hand[2])
				&& (wahrerWertDerKarte(hand[3]) == wahrerWertDerKarte(hand[4]))
				&& (wahrerWertDerKarte(hand[2]) == wahrerWertDerKarte(hand[3]))) {
			return true;
		} else if (wahrerWertDerKarte(hand[0]) == wahrerWertDerKarte(hand[1])
				&& (wahrerWertDerKarte(hand[2]) == wahrerWertDerKarte(hand[4]))
				&& (wahrerWertDerKarte(hand[1]) == wahrerWertDerKarte(hand[2]))) {
			return true;
		}

		return false;
	}

	// hier wird ein Flush gesucht
	public static boolean findeFlush(int karte []) {
		for (int i = 0; i < karte.length - 1; i++) {
			if (wahreFarbeDerKarte(karte[i]) != wahreFarbeDerKarte(karte[i + 1])) {
				return false;
			}
		}

		return true;
	}

	// hier werden 2 Paare gesucht
	public static boolean finde2Paar(int karte []) {
		int erste = 0;
		for (int i = 0; i < karte.length - 1; i++) {
			for (int j = i + 1; j < karte.length; j++) {
				if (wahrerWertDerKarte(karte[i]) == wahrerWertDerKarte(karte[j])) {
					erste = wahrerWertDerKarte(karte[i]);
				}
			}
		}

		for (int i = 0; i < karte.length - 1; i++) {
			for (int j = i + 1; j < karte.length; j++) {
				if (wahrerWertDerKarte(karte[i]) == wahrerWertDerKarte(karte[j])
						&& wahrerWertDerKarte(karte[i]) != erste) {
					return true;
				}
			}
		}
		return false;

	}

	// hier wird ein FullHouse gesucht (1Drilling und 1Paar)
	public static boolean findeFullHouse(int karte []) {
		int ersteKarte = 0;
		boolean found = false;
		for (int i = 0; i < karte.length - 2; i++) {
			for (int j = i + 1; j < karte.length - 1; j++) {
				for (int k = j + 1; k < karte.length; k++) {
					if (wahrerWertDerKarte(karte[i]) == wahrerWertDerKarte(karte[j])
							&& wahrerWertDerKarte(karte[k]) == wahrerWertDerKarte(karte[j])) {
						ersteKarte = wahrerWertDerKarte(karte[k]);
						found = true;
					}
				}
			}
		}
		for (int i = 0; i < karte.length - 1; i++) {
			for (int j = i + 1; j < karte.length; j++) {
				if (wahrerWertDerKarte(karte[i]) == wahrerWertDerKarte(karte[j])
						&& wahrerWertDerKarte(karte[i]) != ersteKarte 
						&& found == true) {
					return true;
				}
			}
		}
		return false;
	}

	// hier wird nach einer Stra�e gesucht
	public static boolean findeStra�e(int karte []) {
		int[] wKarten = new int[5];
		for (int i = 0; i < karte.length; i++) {
			wKarten[i] = wahrerWertDerKarte(karte[i]);
		}
		Arrays.sort(wKarten);
		for (int i = 0; i < wKarten.length - 1; i++) {
			if ((wKarten[i + 1] != wKarten[i] + 1)) {
				return false;
			}
		}
		return true;
	}

	// hier wird eine Stra�e mit nur gleichen Farben gesucht
	public static boolean findeStraightFlush(int karte []) {
		int[] wKarten = new int[5];
		for (int i = 0; i < karte.length; i++) {
			wKarten[i] = wahrerWertDerKarte(karte[i]);
		}
		Arrays.sort(wKarten);
		if (findeFlush(karte) == false || findeStra�e(wKarten) == false) {
			return false;
		}
		return true;
	}

	// hier wird die oberste Stra�e in gleicher Farbe gesucht
	public static boolean findeRoyalFlush(int karte []) {
		int[] wKarten = new int[5];

		for (int i = 0; i < karte.length; i++) {
			wKarten[i] = wahrerWertDerKarte(karte[i]);
		}

		Arrays.sort(wKarten);

		if (findeFlush(karte) == false || (wahrerWertDerKarte(wKarten[4]) != 12) || findeStra�e(wKarten) == false) {
			return false;
		}

		return true;
	}
}
