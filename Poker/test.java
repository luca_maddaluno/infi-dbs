import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class test {
	@Test
	public void testDrilling() {
		int[] karte = {0,13,26,25,28};		
		boolean b = Poker.findeDrilling(karte);
		assertEquals(true,b);
	}
	
	@Test
	public void testFlush() {
		int[] karte = {13,18,17,16,14};		
		boolean b = Poker.findeFlush(karte);
		assertEquals(true,b);
	}
	@Test
	public void testFullHouse() {
		int[] karte = {13,26,39,14,27};		
		boolean b = Poker.findeFullHouse(karte);
		assertEquals(true,b);
	}
	@Test
	public void testPaar() {	
		int[] karte = {0,14,15,16,29};		
		boolean b = Poker.findePaar(karte);
		assertEquals(true,b);
	}
	@Test
	public void testPoker() {	
		int[] karte = {52,13,26,39,40};
		boolean b = Poker.findePoker(karte);
		assertEquals(true,b);
	}
	@Test
	public void testRoyalFlush() {
		int[] karte = {21,24,22,23,25};		
		boolean b = Poker.findeRoyalFlush(karte);
		assertEquals(true,b);
	}
	@Test
	public void testStraightFlush() {		
		int[] karte = {13,14,15,16,17};		
		boolean b = Poker.findeStraightFlush(karte);
		assertEquals(true,b);
	}
	@Test
	public void testStraight() {	
		int[] karte = {26,27,28,29,30};		
		boolean b = Poker.findeStraße(karte);
		assertEquals(true,b);
	}
	@Test
	public void testZweiPaare() {
		int[] karte = {0,15,1,13,14};		
		boolean b = Poker.finde2Paar(karte);
		assertEquals(true,b);
	}

}
