import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class AutoverleihManager 
{
	
	
		Connection c;
		
		public AutoverleihManager() throws ClassNotFoundException, SQLException 
		{
		
		    try {
		      Class.forName("org.sqlite.JDBC");
		      c = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/autoverleih.db");
		      c.setAutoCommit(false);
		      System.out.println("Opened database successfully");
		      
		    } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
		    }
		    create();
		}
		
		public void create() throws SQLException
		{
			c= DriverManager.getConnection("jdbc:sqlite:C:/sqlite/autoverleih.db");
			try{
				
			String sql = "Create Table IF NOT EXISTS Kunden(Email varchar(150) Primary Key NOT NULL,"
					+ "Vorname varchar(100) NOT NULL,Nachname varchar(100) NOT NULL,"
					+ "Passwort varchar(20) NOT NULL,Postleitzahl INT NOT NULL,"
					+ "Stra�e varchar(100) NOT NULL,Hausnummer INT NOT NULL);";
			Statement stmt = c.createStatement();
			stmt.executeUpdate(sql);
			
			sql = "Create Table IF NOT EXISTS Fahrzeugdaten("
					+"FahrzeugID INTEGER Primary Key AUTOINCREMENT,"
					+"Marke varchar(100) NOT NULL,"
					+"Modell varchar(100) NOT NULL,"
					+"Sitzpl�tze INT NOT NULL);";
			stmt = c.createStatement();
			stmt.executeUpdate(sql);
			
			sql = "Create Table IF NOT EXISTS Reparatur("
					+"ReparaturID INTEGER Primary Key AUTOINCREMENT,"
					+"Bezeichnung varchar(100) NOT NULL,"
					+"Kosten DECIMAL NOT NULL,"
					+"FahrzeugID INT NOT NULL,"
					+"Foreign Key(FahrzeugID) References Fahrzeugdaten(FahrzeugID) ON DELETE Cascade);";
			
			stmt = c.createStatement();
			stmt.executeUpdate(sql);
			
			sql = "Create Table IF NOT EXISTS Geliehen("
					+"Email varchar(150) NOT NULL,"
					+"FahrzeugID INT NOT NULL,"
					+"Primary Key (Email, FahrzeugID),"
					+"Foreign Key(Email) References Kunden(Email) On DELETE Cascade,"
					+"Foreign Key (FahrzeugID) References Fahrzeugdaten(FahrzeugID) On DELETE Cascade);";
			
			stmt = c.createStatement();
			stmt.executeUpdate(sql);
			
			stmt.close();
			
			System.out.println("created successfully");
			} 
			catch ( Exception e ) {
			      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			      System.exit(0);
			    }
			c.close();
		}
		
		public void hinzuf�genKunde(Kunde kunde) throws SQLException
		{
			c= DriverManager.getConnection("jdbc:sqlite:C:/sqlite/autoverleih.db");
			String sql = "Insert INTO Kunden Values(?,?,?,?,?,?,?)";
			PreparedStatement stmt = c.prepareStatement(sql);
			stmt.setString(1, kunde.getEmail());
			stmt.setString(2, kunde.getVorname());
			stmt.setString(3, kunde.getNachname());
			stmt.setString(4, kunde.getPasswort());
			stmt.setInt(5, kunde.getPlz());
			stmt.setString(6, kunde.getStra�e());
			stmt.setInt(7, kunde.getHausnummer());
			stmt.executeUpdate();
			stmt.close();
			c.close();
		}
		
		public void hinzuf�genAuto(Fahrzeugdaten f) throws SQLException
		{
			c= DriverManager.getConnection("jdbc:sqlite:C:/sqlite/autoverleih.db");
			String sql = "Insert INTO Fahrzeugdaten Values (?,?,?,?)" ;
			PreparedStatement stmt = c.prepareStatement(sql);
			stmt.setString(2, f.getMarke());
			stmt.setString(3, f.getModell());
			stmt.setInt(4, f.getSitzpl�tze());
			stmt.executeUpdate();
			stmt.close();
			c.close();
		}
		
		public void leihen(int fahrzeugID, String email) throws SQLException
		{
			c= DriverManager.getConnection("jdbc:sqlite:C:/sqlite/autoverleih.db");
			String sql  ="Insert INTO Geliehen VALUES(?,?)";
			PreparedStatement stmt = c.prepareStatement(sql);
			stmt.setString(1, email);
			stmt.setInt(2, fahrzeugID);
			stmt.executeUpdate();
			stmt.close();
			c.close();
		}
		
		public void reparieren(String bezeichnung, int kosten, int fahrzeugID) throws SQLException
		{
			c= DriverManager.getConnection("jdbc:sqlite:C:/sqlite/autoverleih.db");
			String sql  ="Insert INTO Reparatur VALUES(?,?,?,?)";
			PreparedStatement stmt = c.prepareStatement(sql);
			stmt.setString(2, bezeichnung);
			stmt.setInt(3, kosten);
			stmt.setInt(4, fahrzeugID);
			stmt.executeUpdate();
			stmt.close();
			c.close();
		}
}

