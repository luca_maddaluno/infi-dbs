import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.TextField;
import java.awt.Panel;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import java.awt.Color;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.border.LineBorder;

public class Autoverleih extends JFrame {

	private JPanel contentPane;
	private JTextField txtVorname;
	private JTextField txtNachname;
	private JTextField txtPasswort;
	private JTextField txtTagKunde;
	private JTextField txtMonatKunde;
	private JTextField txtJahrKunde;
	private JTextField txtPlz;
	private JTextField txtStra�e;
	private JTextField txtHausnummer;
	private JTextField txtFahrzeugmarke;
	private JTextField txtModell;
	private JTextField txtSitzpl�tze;
	private JTextField txtEmailAusleihen;
	private JTextField txtFahrzeugIdAusleihen;
	private JTextField txtReparaturBezeichnung;
	private JTextField txtKosten;
	private JTextField txtFahrzeugIdReparatur;
	private JTextField txtFahrzeugIDAuto;
	
	Kunde k;
	Fahrzeugdaten f;
	AutoverleihManager m;
	Reparatur r;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Autoverleih frame = new Autoverleih();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public Autoverleih() throws ClassNotFoundException, SQLException {
		
		m=new AutoverleihManager();
		
		setTitle("Autoverleih");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 993, 518);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		TextField txtEmailKunde = new TextField();
		txtEmailKunde.setBounds(108, 10, 145, 24);
		contentPane.add(txtEmailKunde);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(31, 10, 71, 24);
		contentPane.add(lblEmail);
		
		JLabel lblVorname = new JLabel("Vorname");
		lblVorname.setBounds(31, 40, 74, 24);
		contentPane.add(lblVorname);
		
		txtVorname = new JTextField();
		txtVorname.setBounds(109, 41, 145, 22);
		contentPane.add(txtVorname);
		txtVorname.setColumns(10);
		
		JLabel lblNachname = new JLabel("Nachname");
		lblNachname.setBounds(31, 77, 71, 22);
		contentPane.add(lblNachname);
		
		txtNachname = new JTextField();
		txtNachname.setBounds(109, 76, 145, 22);
		contentPane.add(txtNachname);
		txtNachname.setColumns(10);
		
		JLabel lblPasswort = new JLabel("Passwort");
		lblPasswort.setBounds(31, 110, 56, 16);
		contentPane.add(lblPasswort);
		
		txtPasswort = new JTextField();
		txtPasswort.setBounds(109, 112, 145, 22);
		contentPane.add(txtPasswort);
		txtPasswort.setColumns(10);
		
		JLabel lblGeburtdatum = new JLabel("Geburtdatum");
		lblGeburtdatum.setBounds(31, 139, 82, 24);
		contentPane.add(lblGeburtdatum);
		
		txtTagKunde = new JTextField();
		txtTagKunde.setBounds(108, 166, 40, 22);
		contentPane.add(txtTagKunde);
		txtTagKunde.setColumns(10);
		
		txtMonatKunde = new JTextField();
		txtMonatKunde.setBounds(160, 166, 40, 22);
		contentPane.add(txtMonatKunde);
		txtMonatKunde.setColumns(10);
		
		txtJahrKunde = new JTextField();
		txtJahrKunde.setBounds(218, 165, 56, 24);
		contentPane.add(txtJahrKunde);
		txtJahrKunde.setColumns(10);
		
		JLabel lblTag = new JLabel("Tag");
		lblTag.setBounds(119, 147, 56, 16);
		contentPane.add(lblTag);
		
		JLabel lblMonat = new JLabel("Monat");
		lblMonat.setBounds(160, 147, 56, 16);
		contentPane.add(lblMonat);
		
		JLabel lblJahr = new JLabel("Jahr");
		lblJahr.setBounds(229, 147, 56, 16);
		contentPane.add(lblJahr);
		
		JLabel lblPlz = new JLabel("PLZ");
		lblPlz.setBounds(31, 202, 56, 16);
		contentPane.add(lblPlz);
		
		txtPlz = new JTextField();
		txtPlz.setBounds(108, 201, 56, 22);
		contentPane.add(txtPlz);
		txtPlz.setColumns(10);
		
		JLabel lblStrae = new JLabel("Stra\u00DFe");
		lblStrae.setBounds(31, 239, 56, 16);
		contentPane.add(lblStrae);
		
		txtStra�e = new JTextField();
		txtStra�e.setBounds(108, 236, 145, 22);
		contentPane.add(txtStra�e);
		txtStra�e.setColumns(10);
		
		JLabel lblHausnummer = new JLabel("Hausnummer");
		lblHausnummer.setBounds(31, 268, 82, 16);
		contentPane.add(lblHausnummer);
		
		txtHausnummer = new JTextField();
		txtHausnummer.setBounds(108, 265, 40, 22);
		contentPane.add(txtHausnummer);
		txtHausnummer.setColumns(10);
		
		JButton btnKundeHinzufgen = new JButton("Kunde hinzuf\u00FCgen");
		btnKundeHinzufgen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				k = new Kunde(txtEmailKunde.getText(),txtVorname.getText(),txtNachname.getText(),
						txtPasswort.getText(),Integer.parseInt(txtTagKunde.getText()), Integer.parseInt(txtMonatKunde.getText()),
						Integer.parseInt(txtJahrKunde.getText()), Integer.parseInt(txtPlz.getText()), txtStra�e.getText(),
						Integer.parseInt(txtHausnummer.getText()));
				try {
					m.hinzuf�genKunde(k);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnKundeHinzufgen.setBounds(69, 322, 145, 25);
		contentPane.add(btnKundeHinzufgen);
		
		JLabel lblFahrzeugmarke = new JLabel("Fahrzeugmarke");
		lblFahrzeugmarke.setBounds(367, 44, 89, 16);
		contentPane.add(lblFahrzeugmarke);
		
		JLabel lblModell = new JLabel("Modell");
		lblModell.setBounds(377, 80, 56, 16);
		contentPane.add(lblModell);
		
		JLabel lblSitzpltze = new JLabel("Sitzpl\u00E4tze");
		lblSitzpltze.setBounds(377, 110, 56, 16);
		contentPane.add(lblSitzpltze);
		
		txtFahrzeugmarke = new JTextField();
		txtFahrzeugmarke.setBounds(468, 41, 145, 22);
		contentPane.add(txtFahrzeugmarke);
		txtFahrzeugmarke.setColumns(10);
		
		txtModell = new JTextField();
		txtModell.setBounds(468, 77, 144, 22);
		contentPane.add(txtModell);
		txtModell.setColumns(10);
		
		txtSitzpl�tze = new JTextField();
		txtSitzpl�tze.setBounds(469, 107, 40, 22);
		contentPane.add(txtSitzpl�tze);
		txtSitzpl�tze.setColumns(10);
		
		JButton btnAutoHinzufgen = new JButton("Auto hinzuf\u00FCgen");
		btnAutoHinzufgen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				f = new Fahrzeugdaten( txtFahrzeugmarke.getText(),
						txtModell.getText(), Integer.parseInt(txtSitzpl�tze.getText()));
				try {
					m.hinzuf�genAuto(f);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnAutoHinzufgen.setBounds(406, 139, 145, 25);
		contentPane.add(btnAutoHinzufgen);
		
		JLabel lblEmail_1 = new JLabel("Email");
		lblEmail_1.setBounds(367, 204, 56, 16);
		contentPane.add(lblEmail_1);
		
		JLabel lblFahrzeugid = new JLabel("FahrzeugID");
		lblFahrzeugid.setBounds(367, 239, 89, 16);
		contentPane.add(lblFahrzeugid);
		
		txtEmailAusleihen = new JTextField();
		txtEmailAusleihen.setBounds(469, 199, 145, 22);
		contentPane.add(txtEmailAusleihen);
		txtEmailAusleihen.setColumns(10);
		
		txtFahrzeugIdAusleihen = new JTextField();
		txtFahrzeugIdAusleihen.setBounds(469, 236, 56, 22);
		contentPane.add(txtFahrzeugIdAusleihen);
		txtFahrzeugIdAusleihen.setColumns(10);
		
		JButton btnAusleihen = new JButton("ausleihen");
		btnAusleihen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					m.leihen(Integer.parseInt(txtFahrzeugIdAusleihen.getText()), txtEmailAusleihen.getText());
				} catch (NumberFormatException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnAusleihen.setBounds(406, 284, 145, 25);
		contentPane.add(btnAusleihen);
		
		Box horizontalBox = Box.createHorizontalBox();
		horizontalBox.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		horizontalBox.setBounds(326, 177, 361, 4);
		contentPane.add(horizontalBox);
		
		Box verticalBox = Box.createVerticalBox();
		verticalBox.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		verticalBox.setBounds(326, 10, 4, 448);
		contentPane.add(verticalBox);
		
		Box verticalBox_1 = Box.createVerticalBox();
		verticalBox_1.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		verticalBox_1.setBackground(Color.BLACK);
		verticalBox_1.setBounds(683, 10, 4, 448);
		contentPane.add(verticalBox_1);
		
		JLabel lblReparaturBezeichnung = new JLabel("Bezeichnung");
		lblReparaturBezeichnung.setBounds(699, 18, 145, 16);
		contentPane.add(lblReparaturBezeichnung);
		
		txtReparaturBezeichnung = new JTextField();
		txtReparaturBezeichnung.setBounds(779, 12, 184, 22);
		contentPane.add(txtReparaturBezeichnung);
		txtReparaturBezeichnung.setColumns(10);
		
		JLabel lblKosten = new JLabel("Kosten");
		lblKosten.setBounds(709, 47, 56, 16);
		contentPane.add(lblKosten);
		
		txtKosten = new JTextField();
		txtKosten.setBounds(779, 44, 56, 22);
		contentPane.add(txtKosten);
		txtKosten.setColumns(10);
		
		JLabel label = new JLabel("\u20AC");
		label.setBounds(847, 47, 56, 16);
		contentPane.add(label);
		
		JLabel lblFahrzeugid_1 = new JLabel("FahrzeugID");
		lblFahrzeugid_1.setBounds(699, 80, 71, 16);
		contentPane.add(lblFahrzeugid_1);
		
		txtFahrzeugIdReparatur = new JTextField();
		txtFahrzeugIdReparatur.setBounds(779, 77, 56, 22);
		contentPane.add(txtFahrzeugIdReparatur);
		txtFahrzeugIdReparatur.setColumns(10);
		
		JButton btnNewButton = new JButton("Reparatur hinzuf\u00FCgen");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				r = new Reparatur(txtReparaturBezeichnung.getText(), Integer.parseInt(txtKosten.getText()), Integer.parseInt(txtFahrzeugIdReparatur.getText()));
				try {
					m.reparieren(txtReparaturBezeichnung.getText(), Integer.parseInt(txtKosten.getText()), Integer.parseInt(txtFahrzeugIdReparatur.getText()));
				} catch (NumberFormatException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(738, 139, 165, 25);
		contentPane.add(btnNewButton);
		
		JLabel lblFahrzeugid_2 = new JLabel("FahrzeugID");
		lblFahrzeugid_2.setBounds(367, 18, 89, 16);
		contentPane.add(lblFahrzeugid_2);
		
		txtFahrzeugIDAuto = new JTextField();
		txtFahrzeugIDAuto.setBounds(468, 12, 57, 22);
		contentPane.add(txtFahrzeugIDAuto);
		txtFahrzeugIDAuto.setColumns(10);
	}
}
