import java.sql.Date;

public class Reparatur {
	

	String bezeichnung;
	int kosten;
	int fahrzeugID;
	public Reparatur(String bezeichnung, int kosten, int fahrzeugID) {
		super();
		this.bezeichnung = bezeichnung;
		this.kosten = kosten;
		this.fahrzeugID = fahrzeugID;
	}
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public int getKosten() {
		return kosten;
	}
	public void setKosten(int kosten) {
		this.kosten = kosten;
	}
	public int getFahrzeugID() {
		return fahrzeugID;
	}
	public void setFahrzeugID(int fahrzeugID) {
		this.fahrzeugID = fahrzeugID;
	}

}
