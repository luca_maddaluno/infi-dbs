
public class Kunde 
{
	int kundenID;
	String vorname;
	String nachname;
	
	public Kunde(int kundenID, String vorname, String nachname) {
		this.kundenID = kundenID;
		this.vorname = vorname;
		this.nachname = nachname;
	}

	public int getKundenID() {
		return kundenID;
	}

	public void setKundenID(int kundenID) {
		this.kundenID = kundenID;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	
	
}
