import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.TextArea;
import javax.swing.JTextArea;

public class GUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtVorname;
	private JTextField txtNachname;
	private JTextField txtWagonID;
	private JTextField txtKlasse;
	private JTextField txtSitzplatzID;
	private JTextField txtSitzplatz;
	private JTextField txtWagonIDSitzplatz;
	private JTextField txtSitzplatzIDBuchen;
	private JTextField txtKundenIDBuchen;
	private JTextField txtKundenID;
	private JTextArea txtArea;
	
	Kunde k;
	Wagon w;
	Sitzplatz s;
	DatenbankManager dm;
	ConfigManager cm;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public GUI() throws SQLException {
		
		cm = new ConfigManager("config");
		dm = new DatenbankManager(cm.getDatenbank());
		dm.create();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1055, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblKundeEinfgen = new JLabel("Kunde einf\u00FCgen: ");
		lblKundeEinfgen.setBounds(12, 13, 102, 22);
		contentPane.add(lblKundeEinfgen);
		
		JLabel lblVorname = new JLabel("Vorname");
		lblVorname.setBounds(12, 77, 56, 16);
		contentPane.add(lblVorname);
		
		JLabel lblNachname = new JLabel("Nachname");
		lblNachname.setBounds(12, 110, 68, 16);
		contentPane.add(lblNachname);
		
		txtVorname = new JTextField();
		txtVorname.setBounds(80, 74, 116, 22);
		contentPane.add(txtVorname);
		txtVorname.setColumns(10);
		
		txtNachname = new JTextField();
		txtNachname.setBounds(80, 107, 116, 22);
		contentPane.add(txtNachname);
		txtNachname.setColumns(10);
		
		JButton btnKundeEinfgen = new JButton("Kunde einf\u00FCgen");
		btnKundeEinfgen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				k = new Kunde(Integer.parseInt(txtKundenID.getText()), txtVorname.getText(), txtNachname.getText());
				try {
					dm.hinzuf�genKunde(k);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnKundeEinfgen.setBounds(12, 139, 139, 25);
		contentPane.add(btnKundeEinfgen);
		
		JLabel lblWagon = new JLabel("Wagon einf\u00FCgen:");
		lblWagon.setBounds(12, 169, 102, 16);
		contentPane.add(lblWagon);
		
		JLabel lblKlasse = new JLabel("WagonID");
		lblKlasse.setBounds(12, 198, 56, 16);
		contentPane.add(lblKlasse);
		
		txtWagonID = new JTextField();
		txtWagonID.setBounds(80, 195, 116, 22);
		contentPane.add(txtWagonID);
		txtWagonID.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Klasse");
		lblNewLabel.setBounds(12, 227, 56, 16);
		contentPane.add(lblNewLabel);
		
		txtKlasse = new JTextField();
		txtKlasse.setBounds(80, 224, 116, 22);
		contentPane.add(txtKlasse);
		txtKlasse.setColumns(10);
		
		JButton btnWagonHinzufgen = new JButton("Wagon hinzuf\u00FCgen");
		btnWagonHinzufgen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				w = new Wagon(Integer.parseInt(txtWagonID.getText()), Integer.parseInt(txtKlasse.getText()));
				try {
					dm.hinzuf�genWagon(w);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnWagonHinzufgen.setBounds(12, 256, 139, 25);
		contentPane.add(btnWagonHinzufgen);
		
		JLabel lblSitzplatzEinfgen = new JLabel("Sitzplatz einf\u00FCgen:");
		lblSitzplatzEinfgen.setBounds(285, 16, 121, 16);
		contentPane.add(lblSitzplatzEinfgen);
		
		JLabel lblSitzplatzid = new JLabel("SitzplatzID");
		lblSitzplatzid.setBounds(285, 48, 74, 16);
		contentPane.add(lblSitzplatzid);
		
		txtSitzplatzID = new JTextField();
		txtSitzplatzID.setBounds(371, 45, 116, 22);
		contentPane.add(txtSitzplatzID);
		txtSitzplatzID.setColumns(10);
		
		JLabel lblSitzplatz = new JLabel("Sitzplatz");
		lblSitzplatz.setBounds(285, 77, 56, 16);
		contentPane.add(lblSitzplatz);
		
		txtSitzplatz = new JTextField();
		txtSitzplatz.setBounds(371, 74, 116, 22);
		contentPane.add(txtSitzplatz);
		txtSitzplatz.setColumns(10);
		
		JLabel lblWagonid = new JLabel("WagonID");
		lblWagonid.setBounds(285, 110, 56, 16);
		contentPane.add(lblWagonid);
		
		txtWagonIDSitzplatz = new JTextField();
		txtWagonIDSitzplatz.setBounds(371, 107, 116, 22);
		contentPane.add(txtWagonIDSitzplatz);
		txtWagonIDSitzplatz.setColumns(10);
		
		JButton btnSitzplatzHinzufgen = new JButton("Sitzplatz hinzuf\u00FCgen");
		btnSitzplatzHinzufgen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				s = new Sitzplatz(Integer.parseInt(txtSitzplatzID.getText()), txtSitzplatz.getText(), Integer.parseInt(txtWagonIDSitzplatz.getText()));
				try {
					dm.hinzuf�genSitzplatz(s);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnSitzplatzHinzufgen.setBounds(285, 139, 147, 25);
		contentPane.add(btnSitzplatzHinzufgen);
		
		JLabel lblNewLabel_1 = new JLabel("Sitzplatz buchen");
		lblNewLabel_1.setBounds(285, 181, 102, 16);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblSitzplatzid_1 = new JLabel("SitzplatzID");
		lblSitzplatzid_1.setBounds(285, 210, 67, 16);
		contentPane.add(lblSitzplatzid_1);
		
		txtSitzplatzIDBuchen = new JTextField();
		txtSitzplatzIDBuchen.setBounds(364, 207, 116, 22);
		contentPane.add(txtSitzplatzIDBuchen);
		txtSitzplatzIDBuchen.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("KundenID");
		lblNewLabel_2.setBounds(285, 242, 56, 16);
		contentPane.add(lblNewLabel_2);
		
		JLabel label = new JLabel("");
		label.setBounds(285, 282, 56, 16);
		contentPane.add(label);
		
		txtKundenIDBuchen = new JTextField();
		txtKundenIDBuchen.setBounds(364, 239, 116, 22);
		contentPane.add(txtKundenIDBuchen);
		txtKundenIDBuchen.setColumns(10);
		
		JButton btnBuchen = new JButton("Buchen");
		btnBuchen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					s = dm.getSitzplatz(Integer.parseInt(txtSitzplatzIDBuchen.getText()));
					k = dm.getKunde(Integer.parseInt(txtKundenIDBuchen.getText()));
					
					dm.hinzuf�genSitzplatzKunde(s, k);
				} catch (NumberFormatException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnBuchen.setBounds(285, 271, 147, 25);
		contentPane.add(btnBuchen);
		
		JButton btnLeuteImZug = new JButton("Leute im Zug");
		btnLeuteImZug.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					int anzahl = dm.howManyZug();
					txtArea.setText("Insgesamt Leute im Zug: "+anzahl);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnLeuteImZug.setBounds(588, 12, 139, 25);
		contentPane.add(btnLeuteImZug);
		
		JButton btnLeuteProKlasse = new JButton("Leute pro Klasse");
		btnLeuteProKlasse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					ArrayList<Kunde> klasse1 = dm.getKundenKlasse(1);
					ArrayList<Kunde> klasse2 = dm.getKundenKlasse(2);
					
					
					String ausgabe = "";
					
					ausgabe += "1.Klasse: ";
					ausgabe += "\n";
					for(Kunde k11:klasse1)
					{
						ausgabe += "	" + k11.getVorname() + " " + k11.getNachname();
						ausgabe += "\n";
					}
					
					ausgabe += "2.Klasse: ";
					ausgabe += "\n";
					for(Kunde k11:klasse2)
					{
						ausgabe += "	" + k11.getVorname() + " "+ k11.getNachname();
						ausgabe += "\n";
					}
					
					txtArea.setText(ausgabe);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnLeuteProKlasse.setBounds(836, 12, 139, 25);
		contentPane.add(btnLeuteProKlasse);
		
		JLabel lblKundenid = new JLabel("KundenID");
		lblKundenid.setBounds(12, 48, 56, 16);
		contentPane.add(lblKundenid);
		
		txtKundenID = new JTextField();
		txtKundenID.setBounds(80, 45, 116, 22);
		contentPane.add(txtKundenID);
		txtKundenID.setColumns(10);
		
		txtArea = new JTextArea();
		txtArea.setBounds(602, 74, 373, 207);
		contentPane.add(txtArea);
		
		

	}
}
