import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DatenbankManager 
{
	Connection c;
	Statement stmt;
	
	public DatenbankManager(String datenbank) throws SQLException
	{
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + datenbank);
			c.setAutoCommit(false);
		      System.out.println("Opened database successfully");
		      
		    } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);

		    }
		
	}
	
	public void close() throws SQLException
	{
		c.close();
	}
	
	public void create() throws SQLException
	{
		stmt = c.createStatement();
		try{
			
		String sql = "Create Table IF NOT EXISTS Kunden("
				+ "kundenID INTEGER Primary Key, "
				+ "vorname varchar(100) NOT NULL,"
				+ "nachname varchar(100) NOT NULL);";
		 stmt.executeUpdate(sql);
	     c.commit();
		
		sql = "Create Table IF NOT EXISTS Wagon("
				+"wagonID INTEGER Primary Key ,"
				+"klasse INT NOT NULL);";
		stmt.executeUpdate(sql);
		c.commit();
		
		sql = "Create Table IF NOT EXISTS Sitzplatz("
				+"sitzplatzID INTEGER Primary Key,"
				+"sitzplatz varchar(100) NOT NULL,"
				+"wagonID INT NOT NULL,"
				+"Foreign Key(wagonID) References Wagon(wagonID) ON DELETE Cascade);";
		
		stmt.executeUpdate(sql);
		c.commit();
		
		sql = "Create Table IF NOT EXISTS SitzplatzKunde("
				+"sitzplatzID INT NOT NULL,"
				+"kundenID INT NOT NULL,"
				+"Primary Key (sitzplatzID, kundenID),"
				+"Foreign Key(sitzplatzID) References Sitzplatz(sitzplatzID) On DELETE Cascade,"
				+"Foreign Key (kundenID) References Kunden(kundenID) On DELETE Cascade);";
		
		stmt.executeUpdate(sql);
		c.commit();
		
		stmt.close();
		
		System.out.println("created successfully");
		} 
		catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
		    }
	}


	public void hinzuf�genKunde(Kunde kunde) throws SQLException
	{
		String sql = "Insert INTO Kunden Values(?,?,?)";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setInt(1, kunde.getKundenID());
		stmt.setString(2, kunde.getVorname());
		stmt.setString(3, kunde.getNachname());
		stmt.executeUpdate();
		c.commit();
		stmt.close();
	}
	
	public void hinzuf�genSitzplatz(Sitzplatz s) throws SQLException
	{
		String sql = "Insert INTO Sitzplatz Values(?,?,?)";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setInt(1, s.getSitzplatzID());
		stmt.setString(2, s.getSitzplatz());
		stmt.setInt(3, s.getWagonID());
		stmt.executeUpdate();
		c.commit();
		stmt.close();
	}
	
	public void hinzuf�genWagon(Wagon w) throws SQLException
	{
		String sql = "Insert INTO Wagon Values(?,?)";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setInt(1, w.getWagonID());
		stmt.setInt(2, w.getKlasse());;
		stmt.executeUpdate();
		c.commit();
		stmt.close();
	}
	
	public void hinzuf�genSitzplatzKunde(Sitzplatz s, Kunde k) throws SQLException
	{
		String sql = "Insert INTO SitzplatzKunde Values(?,?)";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setInt(1, s.getSitzplatzID());
		stmt.setInt(2, k.getKundenID());
		stmt.executeUpdate();
		c.commit();
		stmt.close();
	}
	
	public Kunde getKunde(int kundenID) throws SQLException
	{
		Kunde k;
		String sql = "Select * from Kunden WHERE kundenID = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setInt(1, kundenID);
		ResultSet rs = stmt.executeQuery();
		rs.next();
		String vorname = rs.getString(2);
		String nachname = rs.getString(3);
		c.commit();
		stmt.close();
		return new Kunde(kundenID, vorname, nachname);
	}
	
	public Sitzplatz getSitzplatz(int sitzplatzID) throws SQLException
	{
		Sitzplatz s;
		String sql = "Select * from Sitzplatz WHERE sitzplatzID = ?";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setInt(1, sitzplatzID);
		ResultSet rs = stmt.executeQuery();
		rs.next();
		String sitzplatz = rs.getString("sitzplatz");
		
		int wagonID = rs.getInt("wagonID");
		c.commit();
		stmt.close();
		return new Sitzplatz(sitzplatzID,sitzplatz,wagonID);
	}
	
	public ArrayList<Kunde> getKundenKlasse(int klasse) throws SQLException
	{
		ArrayList<Kunde> a = new ArrayList<Kunde>();
		String sql = "Select k.kundenID, vorname, nachname from Kunden k, Sitzplatz s, SitzplatzKunde sz, "
				+ "Wagon w WHERE klasse = ? AND w.wagonID=s.wagonID AND s.sitzPlatzID=sz.sitzPlatzID AND "
				+ "sz.kundenID=k.kundenID ";
		PreparedStatement stmt = c.prepareStatement(sql);
		stmt.setInt(1, klasse);
		ResultSet rs = stmt.executeQuery();
		while(rs.next())
		{
			rs.next();
			a.add(new Kunde(rs.getInt(1),rs.getString("vorname"), rs.getString("nachname")));
		}
		
		c.commit();
		stmt.close();
		return a;
	}
	
	public int howManyZug() throws SQLException
	{
		int anzahl = 0;
		String sql = "Select Count(sitzplatzID) From SitzplatzKunde";
		PreparedStatement stmt = c.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		rs.next();
		anzahl = rs.getInt(1);
		return anzahl;
	}
}
