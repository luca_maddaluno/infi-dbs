import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigManager 
{
	private Properties eigenschaften;

	public ConfigManager(String pfad) {

		FileInputStream in = null;

		try {
			in = new FileInputStream(pfad);
			eigenschaften = new Properties();
			eigenschaften.load(in);

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public String getDatenbank() {

		return eigenschaften.getProperty("Datenbankpfad");
	}

}
