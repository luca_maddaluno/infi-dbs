
public class Sitzplatz 
{
	int sitzplatzID;
	String sitzplatz;
	int wagonID;
	
	
	public Sitzplatz(int sitzplatzID, String sitzplatz, int wagonID) {
		super();
		this.sitzplatzID = sitzplatzID;
		this.sitzplatz = sitzplatz;
		this.wagonID = wagonID;
	}


	public int getSitzplatzID() {
		return sitzplatzID;
	}


	public void setSitzplatzID(int sitzplatzID) {
		this.sitzplatzID = sitzplatzID;
	}


	public String getSitzplatz() {
		return sitzplatz;
	}


	public void setSitzplatz(String sitzplatz) {
		this.sitzplatz = sitzplatz;
	}


	public int getWagonID() {
		return wagonID;
	}


	public void setWagonID(int wagonID) {
		this.wagonID = wagonID;
	}
	
	
}
